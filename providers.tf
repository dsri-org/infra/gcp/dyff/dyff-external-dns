# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

provider "cloudflare" {
  api_token = var.cloudflare_api_token
}

provider "google" {
  credentials = file(var.google_cloud_service_account_file)
  project     = var.project
}

provider "helm" {
  kubernetes {
    host  = "https://${data.google_container_cluster.dyff.endpoint}"
    token = data.google_client_config.provider.access_token
    cluster_ca_certificate = base64decode(
      data.google_container_cluster.dyff.master_auth[0].cluster_ca_certificate,
    )
  }
}

provider "kubernetes" {
  host  = "https://${data.google_container_cluster.dyff.endpoint}"
  token = data.google_client_config.provider.access_token
  cluster_ca_certificate = base64decode(
    data.google_container_cluster.dyff.master_auth[0].cluster_ca_certificate,
  )
}
