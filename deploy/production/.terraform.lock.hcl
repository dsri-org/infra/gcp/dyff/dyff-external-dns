# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/cloudflare/cloudflare" {
  version     = "4.26.0"
  constraints = "~> 4.26.0"
  hashes = [
    "h1:w1+uhedEH1oeT9oZffxff+FFDZIKgT6p9A+z/1ISO2k=",
    "zh:086c755d1dd7399b354f391242c9be636d95fc5b91e5a85cbf1e476607dae4dc",
    "zh:2315c1d4a7496225d4b6421498cdfea2e459f66fd98eb0e125dfe44740fbc644",
    "zh:258d2a34f10cff913fd73d56b3ceae863c144b45ba9f8b4c2ddaae74282d146d",
    "zh:27fd7a3edf509079041f09fd413e157f72fbda1827e82324da384c820f366e7e",
    "zh:65d9fd2af262463fdefbb803d310518b3e8f78a8ba20d2b0a2c63b55770f294f",
    "zh:6987c738de0b1fec31545c67de1cda88c4c01f2b5244d7b2e26462a8a1339439",
    "zh:890df766e9b839623b1f0437355032a3c006226a6c200cd911e15ee1a9014e9f",
    "zh:90806d49509f1a02f5503eb98ba0156c9ebec16587e20a0aa4143cb935ad5928",
    "zh:ac3c2e6d5f40b56d7dfcdd6ae49803644cc6e3cc69d7c369c78c7d9f385decc2",
    "zh:acf1177ad5b399accddfb8847134c3b8c1b226bfbc4e3acfe7698e7de61d7cfa",
    "zh:b2991776b8846d5b819c8673879a459a17f68efb199110c43a446d6c0d6ce17c",
    "zh:cd8c44f495f4ab370e59b0c86ff4c0cc4f8853eb4697019ec5aa2164a1c66856",
    "zh:f100d1c9475c071a8766a6b2dfef8318e8eda878726343e7d2f59aec17373f42",
    "zh:f678dc7995387439df5c56c86a295bda29b1f8fffdee749a3d51f231a94e252b",
    "zh:ffce3d977269cb0a600f6b4203537f3587009e44e3ca11c4f6a82bb5fb359d0c",
  ]
}

provider "registry.opentofu.org/hashicorp/google" {
  version     = "5.18.0"
  constraints = "~> 5.18.0"
  hashes = [
    "h1:rwT/Nbyt86WhxJMS24UhECIfe2oYAMEO+MaXk0Uf9ak=",
    "zh:02e755030e5ea87eefda0f8bb3e6b53df2d2578144c92b4daa21d42336386b83",
    "zh:25c3eb7ec7654f4955abcc700d91e9bc7f0ef196508702194351a97dabc087b9",
    "zh:6dc9014d91bcbb21546e561fe465aed9c8a69b4d465414f8723026b4b47abaea",
    "zh:876fcb3c32a72b21e61fcfac34cc6749edb7247628165eda2e2a3f0282f8e6a0",
    "zh:ac6d119350be3a4463cff3f7b6c7d86abeaee279d01c9c344571eff7d945180a",
    "zh:c28036b74c81007770fe97aae594e2d797eeff51a4807bb37be3891df3109194",
    "zh:d06bac750620247344ea2d57196bdd8e1daed68fe0005a028d059e89116dfe31",
    "zh:d4dbeddfe7bb80e0df8d26ff706d8c897e3e82698f3402b2659050704c0789e0",
    "zh:d604b24b5001da7743f4ce5036e4ef17f7cc54478f7cb327144cef3760e05bf3",
    "zh:e1b8a575928c5bbaa7afa7267474a1bc52da4e9ac526343dfbcc3ef95584fe61",
  ]
}

provider "registry.opentofu.org/hashicorp/helm" {
  version     = "2.12.1"
  constraints = "~> 2.12.0"
  hashes = [
    "h1:S0+5VN/viVA4YYpm9q45bZ903EqP3bwjv5abps+a3lE=",
    "zh:0349149992646530c33314cb973eba68757606a037017ba47e56db695d4b3afe",
    "zh:3138ffe23c481b01419a4a21adf83538efe6e698b421c4a8f7d142b198518709",
    "zh:44658e3070405b88fbd76161ecddde62f478dc31aaebee3b93c2f2783a6d45f9",
    "zh:5600a3407dfb8b77da7561490157afa8ad505c864a5dd35ed8d678e9ad8378ca",
    "zh:6445e359c813ecbb7c2edf722ed0d1f33dfb171b6a7b470f40cf1e24045b7441",
    "zh:7973054604c7f5a51600f6e63fa0327d05b29fac2bffd222c21660cbdd2939f9",
    "zh:7c59e2d4602ab5d9de0ba8e442ec1fc425c8f143581018d1e7f645298a124f01",
    "zh:8c0fb411dd5de664ac5e801d70507781790c4fc196518a56966d66d0963c240c",
    "zh:a6a988c91bbf1828a8fc55001f10c7d06c5c53dc718ee7cd6814bdfa2e6652e0",
    "zh:b7935d7dacd7e5a91ff9d17cfb04ce88c9100e563fd88487d14519e8d8d8b2e1",
  ]
}

provider "registry.opentofu.org/hashicorp/kubernetes" {
  version     = "2.27.0"
  constraints = "~> 2.27.0"
  hashes = [
    "h1:Jtbdvbq8kIXUENtH3tVwgcjHqbuYp1pGfg4gFocY+e4=",
    "zh:1146f53fb39fd4bcea5574303c4871001a97d7891f65a60a4ecbc64da2a90d75",
    "zh:1f7e3dc0dbb854f56a0f5ba3c50588272984ae9775da027c3c7f32cb6d8245b0",
    "zh:2166f7fdade75266658603280bc822edab848e52a674340485847dde1c5d9324",
    "zh:21a97530857330d2013aa66fb7afebb44fe4a5543418d0a3ca93750acd11fea5",
    "zh:2d4b9fea7e99750647e1cd8df9a67cba45905825867dd19ab01411dad6b8c6fd",
    "zh:de30e92e638b95e56dbb2232cb9a6f6a69346ecb3644965e9be715eaf29f22ff",
    "zh:f4ae951c9add4349a498f44c3f5768cbaf7a966392a0e7632de288889e7cd5d9",
    "zh:f54ecb1917dfa198933d72632ea6f0aa4da3ead070d6b9765ec1d3b7da60e827",
    "zh:fba8a2f192eb5fe248708b9037db046e0d9176e7c54c6edc6f6aa55d50474082",
    "zh:fe525956f3e54f0bbd2891a6abad1f807b4763b8dc734d810e223876741fefa3",
  ]
}
