# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

module "root" {
  source = "../.."

  cloudflare_api_token              = var.cloudflare_api_token
  environment                       = "production"
  google_cloud_service_account_file = var.google_cloud_service_account_file
  project                           = "production-dyff-862492"
}
