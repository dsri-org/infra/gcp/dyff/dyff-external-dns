# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

data "cloudflare_zone" "dyff" {
  name = local.domain
}

data "google_client_config" "provider" {}

data "google_container_cluster" "dyff" {
  name     = local.cluster_name
  location = local.region
}
