# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

locals {
  deployment   = "dyff-external-dns"
  cluster_name = "${var.environment}-dyff-cloud"
  name         = "${var.environment}-${local.deployment}"
  region       = "us-central1"

  domain = "dyff.io"

  default_tags = {
    deployment  = local.deployment
    environment = var.environment
  }
}
