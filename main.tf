# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "kubernetes_namespace" "external_dns" {
  metadata {
    name = "external-dns"
    labels = {
      # https://kubernetes.io/docs/concepts/security/pod-security-standards/
      "pod-security.kubernetes.io/enforce" = "restricted"
    }
  }
}

# https://artifacthub.io/packages/helm/bitnami/external-dns
# https://github.com/kubernetes-sigs/external-dns/blob/master/docs/tutorials/cloudflare.md
# https://github.com/bitnami/charts/blob/master/bitnami/external-dns/values.yaml
resource "helm_release" "external_dns" {
  name       = "external-dns"
  namespace  = kubernetes_namespace.external_dns.metadata.0.name
  repository = "oci://registry-1.docker.io/bitnamicharts"
  chart      = "external-dns"
  version    = "7.0.0"

  skip_crds = true

  wait          = false
  wait_for_jobs = false

  values = [yamlencode({
    domainFilters = [local.domain]
    policy        = "sync"
    provider      = "cloudflare"

    # This is required to prevent the different environments from destroying
    # each other's entries.
    txtOwnerId = local.name

    zoneIdFilters = [data.cloudflare_zone.dyff.zone_id]

    cloudflare = {
      proxied = true
    }
  })]

  set_sensitive {
    name  = "cloudflare.apiToken"
    value = var.cloudflare_api_token
  }
}
